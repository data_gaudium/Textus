Smoky Mountain Data Challenge
==============================

This group was synthesized to solve a series of challenge problems presented in the `Smoky Mountain Data Challenge`_ .

.. _Smoky Mountain Data Challenge: https://smc-datachallenge.ornl.gov/challenges-2018/


.. author:: default
.. categories:: none
.. tags:: none
.. comments::

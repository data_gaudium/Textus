About
=====

The Data Gaudium is a collective of aspiring data scientists and engineers.


Members
-------

Austin Ellis
^^^^^^^^^^^^

Austin is a PhD candidate at North Carolina State.

William Gurecky (`CV`_)
^^^^^^^^^^^^^^^^^^^^^^^

William is a PhD candidate in the Nuclear and Radiation Engineering department at the University of Texas at Austin.

.. _CV: https://wgurecky.github.io/blog/html/_downloads/resume_gurecky.pdf

Zack Taylor
^^^^^^^^^^^

Zach is seeking his masters degree in chemical engineering at the University of Tennessee in Knoxville.
